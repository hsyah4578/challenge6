package org.binar.challenge6.service;

import org.binar.challenge6.model.Seats;
import org.binar.challenge6.repository.SeatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeatsServiceImpl implements SeatsService{

    @Autowired
    private SeatsRepository seatsRepository;

    @Override
    public Seats saveSeat(Seats seats) {
        return seatsRepository.save(seats);
    }

    @Override
    public Iterable<Seats> getAllSeats() {
        return seatsRepository.findAll();
    }
}
