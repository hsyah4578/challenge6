package org.binar.challenge6.service;

import org.binar.challenge6.model.Schedules;

import java.util.List;

public interface SchedulesService {

    Schedules saveSchedule (Schedules schedule);

    void removeScheduleByScheduleId (Integer scheduleId);

    List<Schedules> findScheduleByTanggalTayang(String tanggalTayang);

    List<Schedules> findScheduleByJudulFilm(String judulFilm);

}
