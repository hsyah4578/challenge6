package org.binar.challenge6.service;

import org.binar.challenge6.model.Schedules;
import org.binar.challenge6.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchedulesServiceImpl implements SchedulesService{

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Override
    public Schedules saveSchedule(Schedules schedule) {
        return schedulesRepository.save(schedule);
    }

    @Override
    public void removeScheduleByScheduleId(Integer scheduleId) {
        schedulesRepository.deleteById(scheduleId);
    }

    @Override
    public List<Schedules> findScheduleByTanggalTayang(String tanggalTayang) {
        return schedulesRepository.findScheduleByTanggalTayang(tanggalTayang);
    }

    @Override
    public List<Schedules> findScheduleByJudulFilm(String judulFilm) {
        return schedulesRepository.findScheduleByJudulFilm(judulFilm);
    }


}
