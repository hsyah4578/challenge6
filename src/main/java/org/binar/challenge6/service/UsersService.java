package org.binar.challenge6.service;

import org.binar.challenge6.model.Users;

public interface UsersService {

        Users saveUser (Users user);

        void removeUserByUserName (String userName);

        Iterable <Users> findAllUser ();

        Users findUserByUserName (String userName);

        Users findUserByEmail (String email);

}
