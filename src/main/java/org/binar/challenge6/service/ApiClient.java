package org.binar.challenge6.service;

import org.springframework.http.ResponseEntity;

public interface ApiClient {
    public ResponseEntity getCurrentPrice();
}
