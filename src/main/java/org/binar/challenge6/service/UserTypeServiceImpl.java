package org.binar.challenge6.service;

import org.binar.challenge6.model.UserType;
import org.binar.challenge6.repository.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeServiceImpl implements UserTypeService {

    @Autowired
    private UserTypeRepository userTypeRepository;

    @Override
    public UserType saveUserType(UserType userType) {
        return userTypeRepository.save(userType);
    }


    @Override
    public void removeById(Integer typeId) {
        userTypeRepository.deleteById(typeId);
    }

    @Override
    public Iterable<UserType> findAllUserType() {
        return userTypeRepository.findAll();
    }

    @Override
    public UserType findByTypeId(Integer typeId) {
        return userTypeRepository.findByTypeId(typeId);
    }

    @Override
    public UserType findByTypeName(String typeName) {
        return userTypeRepository.findByTypeName(typeName);
    }

}
