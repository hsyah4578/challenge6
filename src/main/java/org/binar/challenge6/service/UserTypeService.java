package org.binar.challenge6.service;

import org.binar.challenge6.model.UserType;

public interface UserTypeService {

    UserType saveUserType(UserType userType);

    void removeById (Integer typeId);

    Iterable <UserType> findAllUserType();

    UserType findByTypeId (Integer typeId);

    UserType findByTypeName (String typeName);

}
