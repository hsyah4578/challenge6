package org.binar.challenge6.service;

import org.binar.challenge6.model.TicketBooking;

public interface TicketBookingService {

    TicketBooking saveTicket(TicketBooking ticketBooking);

    Iterable<TicketBooking> findAllTickets();

}
