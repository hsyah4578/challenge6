package org.binar.challenge6.service;

import org.binar.challenge6.model.Seats;

public interface SeatsService {

    Seats saveSeat (Seats seats);

    Iterable<Seats> getAllSeats();

}
