package org.binar.challenge6.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.binar.challenge6.model.TicketBooking;
import org.binar.challenge6.service.TicketBookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Invoice", description = "API for processing with Invoice entity")
@RestController
@RequestMapping("/ticket")
@AllArgsConstructor
public class TicketBookingController {

    @Autowired
    private TicketBookingService ticketBookingService;

    @Operation(summary = "add tiket booking to the Invoice table in Bioskop Database")
    @PostMapping("/add-ticket")
    public TicketBooking saveTiket (TicketBooking ticketBooking){
        return ticketBookingService.saveTicket(ticketBooking);
    }

    @Operation(summary = "show all tiket in the Invoice table in Bioskop Database")
    @GetMapping
    public Iterable <TicketBooking> findAll (){
        return ticketBookingService.findAllTickets();
    }


}
