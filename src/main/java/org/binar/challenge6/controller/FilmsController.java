package org.binar.challenge6.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.binar.challenge6.model.Films;
import org.binar.challenge6.service.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Films", description = "API for processing with Films entity")
@RestController
@RequestMapping("/films")
@AllArgsConstructor
public class FilmsController {

    @Autowired
    private FilmsService filmsService;

    @Operation(summary = "add film to the Films table in Bioskop Database")
    @PostMapping("/add-film")
    public Films addFilm (@RequestBody Films film){
        return filmsService.saveFilm(film);
    }

    @Operation(summary = "delete film by kodeFilm in the Films table in Bioskop Database")
    @DeleteMapping("/{kodeFilm}")
    public String deleteFilm (@PathVariable("kodeFilm") String kodeFilm){
        filmsService.removeFilmByKodeFilm(kodeFilm);
        return "Hapus data film berhasil";
    }

    @Operation(summary = "get all films in the Films table in Bioskop Database")
    @GetMapping
    public Iterable <Films> findAllFilms (){
        return filmsService.findAllFilms();
    }

    @Operation(summary = "get film by kodeFilm from the Films table in Bioskop Database")
    @GetMapping("/{kodeFilm}")
    public Films findFilmByKodeFilm (@PathVariable("kodeFilm") String kodeFilm){
        return filmsService.findFilmByKodeFilm(kodeFilm);
    }

    @Operation(summary = "get films by sedang tayang from the Films table in Bioskop Database")
    @GetMapping("/tayang")
    public List<Films> findFilmBySedangTayang (){
        return filmsService.findFilmBySedangTayang();
    }

}
