package org.binar.challenge6.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class TicketBooking implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer nomorTiket;

    @ManyToOne
    @JoinColumn(name = "userName", nullable = false)
    private Users userName;

    @ManyToOne
    @JoinColumn(name = "scheduleId", nullable = false)
    private Schedules scheduleId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "nomorKursi", referencedColumnName = "nomorKursi", nullable = false),
            @JoinColumn(name = "studio", referencedColumnName = "studio", nullable = false)
    })
    private Seats pesan;
}
