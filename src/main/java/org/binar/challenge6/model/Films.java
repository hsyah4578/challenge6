package org.binar.challenge6.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Setter
@Getter
@Entity
public class Films implements Serializable {

    @Id
    private String kodeFilm;

    private String judulFilm;

    private Boolean sedangTayang;

}
