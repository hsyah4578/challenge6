package org.binar.challenge6.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Setter
@Getter
@Entity
public class UserType implements Serializable {

    @Id
    private Integer typeId;

    @Column(unique = true)
    private String typeName;

}
