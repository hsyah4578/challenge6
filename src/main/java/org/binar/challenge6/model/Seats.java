package org.binar.challenge6.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Getter
@Setter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@IdClass(StudioSeat.class)
public class Seats {

    @Id
    private Integer nomorKursi;
    @Id
    private String studio;

}
