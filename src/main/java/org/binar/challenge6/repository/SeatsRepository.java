package org.binar.challenge6.repository;

import org.binar.challenge6.model.Seats;
import org.binar.challenge6.model.StudioSeat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeatsRepository extends JpaRepository<Seats, StudioSeat> {

}
