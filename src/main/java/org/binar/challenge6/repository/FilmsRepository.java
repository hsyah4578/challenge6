package org.binar.challenge6.repository;

import org.binar.challenge6.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmsRepository extends JpaRepository <Films, String> {
    @Query("select f from Films f where f.kodeFilm = :kodeFilm")
    Films findFilmByKodeFilm (@Param("kodeFilm") String kodeFilm);

    @Query("select f from Films f where f.judulFilm = :judulFilm")
    Films findFilmByjudulFilm (@Param("judulFilm") String judulFilm);

    @Query("select f from Films f where f.sedangTayang = true")
    List <Films> findFilmBySedangTayang ();
}
